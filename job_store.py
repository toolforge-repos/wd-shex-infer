# -*- coding: utf-8 -*-

import bz2
from datetime import datetime, timezone
import gzip
from io import StringIO
import lzma
from os import link, path
import shutil

from job import Job, null_job


class SqlJobStore:
    def __init__(self, connection):
        self.connection = connection
        with self.connection.cursor() as cursor:
            try:
                cursor.execute("SELECT 1 FROM job LIMIT 1")
            except cursor.ProgrammingError:
                # table does not exist yet
                sql = """CREATE TABLE job (
                             id int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
                             author_name varchar(255) binary NOT NULL,
                             title varchar(255) binary NOT NULL,
                             description varchar(4095) DEFAULT NULL,
                             url varchar(4095) binary DEFAULT NULL,
                             ts_created int unsigned NOT NULL,
                             ts_last_running int unsigned NOT NULL,
                             ts_first_stopped int unsigned DEFAULT NULL,
                             input_sparql text NOT NULL,
                             output_shex text DEFAULT NULL,
                             output_stdout text DEFAULT NULL,
                             output_stderr text DEFAULT NULL
                         )"""
                cursor.execute(sql)
                self.connection.commit()

                sql = """CREATE INDEX job_ts_first_stopped ON job (ts_first_stopped, id)"""
                cursor.execute(sql)
                self.connection.commit()

    def _read(self, io):
        if io is None:
            return None
        else:
            ret = io.read()
            io.seek(0)
            return ret

    def _write(self, text):
        if text is None:
            return None
        else:
            ret = StringIO(text)
            return ret

    def _datetime(self, timestamp):
        if timestamp:
            return datetime.fromtimestamp(timestamp, timezone.utc)
        else:
            return None

    def job_new(self, job):
        assert job.id is None
        with self.connection.cursor() as cursor:
            sql = """INSERT INTO job
                     (author_name, title, description, url, ts_created, ts_last_running, input_sparql)
                     VALUES (%s, %s, %s, %s, %s, %s, %s)"""
            ts = datetime.now(timezone.utc).timestamp()
            cursor.execute(sql, (job.author_name, job.title, job.description, job.url, ts, ts, self._read(job.input_sparql)))
            job = job.created(cursor.lastrowid, ts)
        self.connection.commit()
        return job

    def get_job(self, id):
        with self.connection.cursor() as cursor:
            sql = """SELECT author_name, title, description, url,
                            ts_created, ts_last_running, ts_first_stopped,
                            input_sparql,
                            output_shex, output_stdout, output_stderr
                     FROM job
                     WHERE id = %s"""
            cursor.execute(sql, (id,))
            result = cursor.fetchone()
            if result is None:
                return None
            return null_job._replace(id=id,
                                     author_name=result['author_name'],
                                     title=result['title'],
                                     description=result['description'],
                                     url=result['url'],
                                     datetime_created=self._datetime(result['ts_created']),
                                     datetime_last_running=self._datetime(result['ts_last_running']),
                                     datetime_first_stopped=self._datetime(result['ts_first_stopped']),
                                     input_sparql=self._write(result['input_sparql']),
                                     output_shex=self._write(result['output_shex']),
                                     output_stdout=self._write(result['output_stdout']),
                                     output_stderr=self._write(result['output_stderr']))

    def get_pending_jobs(self):
        jobs = []
        with self.connection.cursor() as cursor:
            sql = """SELECT id,
                            author_name, title, description, url,
                            ts_created, ts_last_running,
                            input_sparql
                     FROM job
                     WHERE ts_first_stopped IS NULL
                     ORDER BY id"""
            cursor.execute(sql)
            while True:
                results = cursor.fetchmany()
                if not results:
                    break
                for result in results:
                    jobs.append(null_job._replace(id=result['id'],
                                                  author_name=result['author_name'],
                                                  title=result['title'],
                                                  description=result['description'],
                                                  url=result['url'],
                                                  datetime_created=self._datetime(result['ts_created']),
                                                  datetime_last_running=self._datetime(result['ts_last_running'])))
        return jobs

    def get_finished_jobs(self):
        jobs = []
        with self.connection.cursor() as cursor:
            sql = """SELECT id,
                     author_name, title, description, url,
                     ts_created, ts_last_running, ts_first_stopped,
                     input_sparql,
                     output_shex, output_stdout, output_stderr
                     FROM job
                     WHERE ts_first_stopped IS NOT NULL
                     ORDER BY id"""
            cursor.execute(sql)
            while True:
                results = cursor.fetchmany()
                if not results:
                    break
                for result in results:
                    jobs.append(null_job._replace(id=result['id'],
                                                  author_name=result['author_name'],
                                                  title=result['title'],
                                                  description=result['description'],
                                                  url=result['url'],
                                                  datetime_created=self._datetime(result['ts_created']),
                                                  datetime_last_running=self._datetime(result['ts_last_running']),
                                                  datetime_first_stopped=self._datetime(result['ts_first_stopped']),
                                                  input_sparql=self._write(result['input_sparql']),
                                                  output_shex=self._write(result['output_shex']),
                                                  output_stdout=self._write(result['output_stdout']),
                                                  output_stderr=self._write(result['output_stderr'])))
        return jobs

    def job_running(self, job):
        with self.connection.cursor() as cursor:
            sql = """UPDATE job
                     SET ts_last_running = %s
                     WHERE id = %s"""
            ts = datetime.now(timezone.utc).timestamp()
            cursor.execute(sql, (ts, job.id))
            job = job.running(ts)
        self.connection.commit()
        return job

    def job_stopped(self, job, output_shex, output_stdout, output_stderr):
        with self.connection.cursor() as cursor:
            sql = """UPDATE job
                     SET ts_first_stopped = %s,
                         output_shex = %s,
                         output_stdout = %s,
                         output_stderr = %s
                     WHERE id = %s"""
            ts = datetime.now(timezone.utc).timestamp()
            cursor.execute(sql, (ts, self._read(output_shex), self._read(output_stdout), self._read(output_stderr), job.id))
            job = job.stopped(ts, output_shex, output_stdout, output_stderr)
        self.connection.commit()
        return job


class LocalFileJobStore:
    def __init__(self, job_store, directory):
        self.job_store = job_store
        self.directory = directory

    def _store_local_file(self, source_io, destination_name):
        destination_path = path.join(self.directory, destination_name)

        try:
            link(source_io.name, destination_path)
            return StringIO(''), open(destination_path, 'r', encoding='utf-8')
        except (AttributeError, OSError):
            pass

        try:
            shutil.copy2(source_io.name, destination_path)
            return StringIO(''), open(destination_path, 'r', encoding='utf-8')
        except (AttributeError, OSError):
            pass

        if isinstance(source_io, StringIO):
            with open(destination_path, 'w', encoding='utf-8') as destination_io:
                destination_io.write(source_io.getvalue())
            return StringIO(''), open(destination_path, 'r', encoding='utf-8')

        return source_io, source_io

    def _move_local_file(self, source_io, destination_name):
        destination_path = path.join(self.directory, destination_name)
        try:
            shutil.move(source_io.name, destination_path)
            return open(destination_path, 'r', encoding='utf-8')
        except AttributeError:
            return source_io

    def _load_local_file(self, source_io, source_name):
        if source_io is None:
            return None
        for suffix, open_func in [('', open),
                                  ('.gz', gzip.open),
                                  ('.bz2', bz2.open),
                                  ('.xz', lzma.open)]:
            try:
                source_path = path.join(self.directory, source_name + suffix)
                return open_func(source_path, 'rt', encoding='utf-8')
            except FileNotFoundError:
                continue
            except OSError:
                return source_io
        return source_io

    def job_new(self, job):
        fake_sparql, real_sparql = self._store_local_file(job.input_sparql, 'NEW.sparql')
        fake_job = job._replace(input_sparql=fake_sparql)
        fake_job_new = self.job_store.job_new(fake_job)
        real_sparql = self._move_local_file(real_sparql, '%d.sparql' % fake_job_new.id)
        real_job_new = fake_job_new._replace(input_sparql=real_sparql)
        return real_job_new

    def get_job(self, id):
        fake_job = self.job_store.get_job(id)
        if fake_job is None:
            return None
        real_sparql = self._load_local_file(fake_job.input_sparql, '%d.sparql' % id)
        real_shex = self._load_local_file(fake_job.output_shex, '%d.shex' % id)
        real_stdout = self._load_local_file(fake_job.output_stdout, '%d.out' % id)
        real_stderr = self._load_local_file(fake_job.output_stderr, '%d.err' % id)
        real_job = fake_job._replace(input_sparql=real_sparql,
                                     output_shex=real_shex,
                                     output_stdout=real_stdout,
                                     output_stderr=real_stderr)
        return real_job

    def get_pending_jobs(self):
        fake_jobs = self.job_store.get_pending_jobs()
        real_jobs = []
        for fake_job in fake_jobs:
            real_sparql = self._load_local_file(fake_job.input_sparql, '%d.sparql' % fake_job.id)
            real_job = fake_job._replace(input_sparql=real_sparql)
            real_jobs.append(real_job)
        return real_jobs

    def get_finished_jobs(self):
        fake_jobs = self.job_store.get_finished_jobs()
        real_jobs = []
        for fake_job in fake_jobs:
            real_sparql = self._load_local_file(fake_job.input_sparql, '%d.sparql' % fake_job.id)
            real_shex = self._load_local_file(fake_job.output_shex, '%d.shex' % fake_job.id)
            real_stdout = self._load_local_file(fake_job.output_stdout, '%d.out' % fake_job.id)
            real_stderr = self._load_local_file(fake_job.output_stderr, '%d.err' % fake_job.id)
            real_job = fake_job._replace(input_sparql=real_sparql,
                                         output_shex=real_shex,
                                         output_stdout=real_stdout,
                                         output_stderr=real_stderr)
            real_jobs.append(real_job)
        return real_jobs

    def job_running(self, job):
        return self.job_store.job_running(job)

    def job_stopped(self, job, output_shex, output_stdout, output_stderr):
        if output_shex is None:
            fake_shex, real_shex = None, None
        else:
            fake_shex, real_shex = self._store_local_file(output_shex, '%d.shex' % job.id)
        fake_stdout, real_stdout = self._store_local_file(output_stdout, '%d.out' % job.id)
        fake_stderr, real_stderr = self._store_local_file(output_stderr, '%d.err' % job.id)
        fake_job = job._replace(output_shex=fake_shex,
                                output_stdout=fake_stdout,
                                output_stderr=fake_stderr)
        fake_job_stopped = self.job_store.job_stopped(fake_job, fake_shex, fake_stdout, fake_stderr)
        real_job_stopped = fake_job_stopped._replace(output_shex=real_shex,
                                                     output_stdout=real_stdout,
                                                     output_stderr=real_stderr)
        return real_job_stopped
