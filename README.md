# Wikidata Shape Expressions Inference

[This tool](https://wd-shex-infer.toolforge.org/) automatically infers ShEx schemas from a set of exemplary Wikidata items.
For more information, see the [documentation page](https://www.wikidata.org/wiki/User:Lucas_Werkmeister/Wikidata_Shape_Expressions_Inference).

## Updating

To update the Toolforge installation, run the following commands:

```
cd ~/www/python/src
git fetch
git log -p @..@{u} # inspect changes
git rebase
```

If there were any changes in the Python dependencies, run:

```
webservice shell
source ~/www/python/venv/bin/activate
pip-sync ~/www/python/src/requirements.txt
exit
```

Then (whether the dependencies needed updating or not), run:

```
webservice restart
```

## Installation

To install the tool from scratch on Toolforge,
start by creating the tool’s container image using the [Toolforge Build Service](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service);
it’s not used to run the webservice,
but for all the setup steps and to later run jobs.

```
toolforge build start https://gitlab.wikimedia.org/toolforge-repos/wd-shex-infer
```

Then, use this image to run all of the `setup-*` commands found in the `Procfile`,
in the order in which they appear in the `Procfile`.

```
toolforge jobs run --wait --image tool-wd-shex-infer/tool-wd-shex-infer:latest --command setup-mkdir{,}
toolforge jobs run --wait --image tool-wd-shex-infer/tool-wd-shex-infer:latest --command setup-install-jena{,}
# ...
```

(The above commands assume you built the image in the `wd-shex-infer` tool;
if you’re in a different tool, you should still build from the same Git repository in the previous step,
but in this step you need to change the image name to match the tool where you built it.)

Configure the `config.yaml` based on `config.yaml.example`,
especially the `kubernetes` section;
replacing `MY-TOOL` with the tool name (e.g. `wd-shex-infer`) should mostly work.
Start the webservice with `webservice start`.

(TODO: The `Procfile` has a `web` command, and we even install GUnicorn into the venv for it,
but we don’t actually use it for the webservice.
Might be something to look into later, perhaps…
I don’t think there’s any strong reason *not* to use it for the webservice?)
