# -*- coding: utf-8 -*-

from io import StringIO
import shlex
import kubernetes
from os import path, remove
import shutil


def jobname(job):
    return 'wd-shex-infer-%d' % job.id


class KubernetesJobsRunner:
    def __init__(self, config):
        self.config = config
        self.directory = path.join(config['tool_data_dir'], 'k8sroot/RDF2Graph')
        self.k8s_configuration = kubernetes.client.configuration.Configuration()
        kubernetes.config.load_kube_config(client_configuration=self.k8s_configuration)
        self.k8s_client = kubernetes.client.ApiClient(configuration=self.k8s_configuration)
        self.k8s_batch = kubernetes.client.BatchV1Api(api_client=self.k8s_client)

    def run(self, job):
        with open(path.join(self.directory, '%d.entities.sparql' % job.id), 'w', encoding='utf-8') as entities_sparql:
            shutil.copyfileobj(job.input_sparql, entities_sparql)
        job.input_sparql.seek(0)
        name = jobname(job)
        k8s_container = kubernetes.client.V1Container(
            name=name,
            image=self.config['image'],
            command=[
                'sh',
                '-c',
                '. /layers/fagiani_apt/apt/.profile.d/000_apt.sh; '
                f'export PATH="$PATH:$TOOL_DATA_DIR/k8sroot/bin"; '
                f'exec make {job.id}.shex >"$TOOL_DATA_DIR"/{shlex.quote(name)}.out 2>"$TOOL_DATA_DIR"/{shlex.quote(name)}.err',
            ],
            working_dir=self.directory,
            resources=kubernetes.client.V1ResourceRequirements(
                requests=self.config.get('requests', {}),
                limits=self.config.get('requests', {}) | self.config.get('limits', {}),
            ),
            volume_mounts=[
                kubernetes.client.V1VolumeMount(
                    name=name,
                    mount_path=path,
                    read_only=False,
                )
                for name, path in self.config.get('mounts', {}).items()
            ],
        )
        k8s_template = kubernetes.client.V1PodTemplateSpec(
            spec=kubernetes.client.V1PodSpec(
                containers=[k8s_container],
                restart_policy='Never',
                volumes=[
                    kubernetes.client.V1Volume(
                        name=name,
                        host_path=kubernetes.client.V1HostPathVolumeSource(path=path),
                    )
                    for name, path in self.config.get('mounts', {}).items()
                ],
            ),
            metadata=kubernetes.client.V1ObjectMeta(labels={
                'app': name,
                'toolforge': 'tool', # required for NFS mounts
            }),
        )
        k8s_spec = kubernetes.client.V1JobSpec(
            template=k8s_template,
            backoff_limit=0,
        )
        k8s_job = kubernetes.client.V1Job(
            api_version='batch/v1',
            kind='Job',
            metadata=kubernetes.client.V1ObjectMeta(name=name),
            spec=k8s_spec,
        )
        self.k8s_batch.create_namespaced_job(
            body=k8s_job,
            namespace=self.config['namespace'],
        )

    def get_output(self, job):
        name = jobname(job)
        try:
            # read_namespaced_job_status() would be nicer but doesn’t work yet (T357172)
            k8s_job = self.k8s_batch.read_namespaced_job(
                name=name,
                namespace=self.config['namespace'],
            )
        except kubernetes.client.exceptions.ApiException as e:
            if e.status != 404:
                print(e)
            k8s_job = None # treat as completed, one way or another
        if k8s_job and not (k8s_job.status.succeeded or k8s_job.status.failed):
            return None # job is still running
        # job completed (possibly already garbage-collected)
        try:
            output_shex = open(path.join(self.directory, '%d.shex' % job.id), 'r', encoding='utf-8')
        except FileNotFoundError:
            output_shex = None
        try:
            output_stdout = open(path.expanduser('~/wd-shex-infer-%d.out' % job.id), 'r', encoding='utf-8')
        except FileNotFoundError:
            output_stdout = StringIO('')
        try:
            output_stderr = open(path.expanduser('~/wd-shex-infer-%d.err' % job.id), 'r', encoding='utf-8')
        except FileNotFoundError:
            output_stderr = StringIO('')
        return output_shex, output_stdout, output_stderr

    def clean(self, job):
        cleanups = [(remove, path.join(self.directory, '%d.entities.sparql' % job.id)),
                    (remove, path.join(self.directory, '%d.nt' % job.id)),
                    (shutil.rmtree, path.join(self.directory, '%d-fuseki' % job.id)),
                    (shutil.rmtree, path.join(self.directory, '%d-results' % job.id)),
                    (remove, path.join(self.directory, '%d.shex' % job.id)),
                    (remove, path.expanduser('~/wd-shex-infer-%d.out' % job.id)),
                    (remove, path.expanduser('~/wd-shex-infer-%d.err' % job.id))]
        for function, argument in cleanups:
            try:
                function(argument)
            except FileNotFoundError:
                pass
        name = jobname(job)
        try:
            self.k8s_batch.delete_namespaced_job(name, self.config['namespace'])
        except kubernetes.client.exceptions.ApiException as e:
            if e.status != 404:
                print(e)
