# -*- coding: utf-8 -*-

import collections
from datetime import datetime, timezone


_Job = collections.namedtuple('Job', ['id',
                                      'author_name',
                                      'title',
                                      'description',
                                      'url',
                                      'datetime_created',
                                      'datetime_last_running',
                                      'datetime_first_stopped',
                                      'input_sparql',
                                      'output_shex',
                                      'output_stdout',
                                      'output_stderr'])


class Job(_Job):
    @property
    def ts_created(self):
        if self.datetime_created:
            return self.datetime_created.timestamp()
        else:
            return None
    @property
    def ts_last_running(self):
        if self.datetime_last_running:
            return self.datetime_last_running.timestamp()
        else:
            return None
    @property
    def ts_first_stopped(self):
        if self.datetime_first_stopped:
            return self.datetime_first_stopped.timestamp()
        else:
            return None

    def created(self, id, ts):
        assert self.id is None
        assert self.datetime_created is None
        assert self.datetime_last_running is None
        dt = datetime.fromtimestamp(ts, timezone.utc)
        return self._replace(id=id,
                             datetime_created=dt,
                             datetime_last_running=dt)

    def running(self, ts):
        assert self.datetime_last_running is not None
        dt = datetime.fromtimestamp(ts, timezone.utc)
        return self._replace(datetime_last_running=dt)

    def stopped(self, ts, output_shex, output_stdout, output_stderr):
        assert self.ts_first_stopped is None
        dt = datetime.fromtimestamp(ts, timezone.utc)
        return self._replace(datetime_first_stopped=dt,
                             output_shex=output_shex,
                             output_stdout=output_stdout,
                             output_stderr=output_stderr)


null_job = Job(id=None,
               author_name=None,
               title=None,
               description=None,
               url=None,
               datetime_created=None,
               datetime_last_running=None,
               datetime_first_stopped=None,
               input_sparql=None,
               output_shex=None,
               output_stdout=None,
               output_stderr=None)
