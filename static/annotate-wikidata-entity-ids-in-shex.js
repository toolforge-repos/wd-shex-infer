'use strict';
async function annotateWikidataEntityIdsInShex(shexElement, languageCodes = ['en']) {
    if (shexElement.childElementCount) {
        console.warn(`ShEx element already has ${shexElement.childElementCount} child elements, adding Wikidata annotations might break them!`);
    }
    const shexHTML = shexElement.innerHTML,
          entityIdsRegexp = /\b[PQ][1-9][0-9]*\b/g;
    let entityIds = shexHTML.match(entityIdsRegexp);
    entityIds = [...new Set(entityIds)];
    const labels = new Map();
    let someEntityIds;
    while ((someEntityIds = entityIds.splice(0, 50)).length) {
        const url = 'https://www.wikidata.org/w/api.php?' +
              'action=wbgetentities&' +
              'ids=' + someEntityIds.join('|') + '&' +
              'props=labels&' +
              'origin=*&' +
              'format=json&formatversion=2';
        const { entities } = await fetch(url, { mode: 'cors', credentials: 'omit' }).then(response => response.json());
        for (const [entityId, entity] of Object.entries(entities)) {
            if (Object.keys(entity.labels || {}).length === 0) {
                console.warn(`No labels for ${entityId}, using entity ID instead!`);
                labels.set(entityId, entityId);
                continue;
            }
            for (const languageCode of languageCodes) {
                if (languageCode in entity.labels) {
                    labels.set(entityId, entity.labels[languageCode].value);
                    break;
                }
            }
            if (!labels.has(entityId)) {
                for (const [languageCode, label] of Object.entries(entity.labels)) {
                    console.warn(`No ${languageCodes.join('/')} label for ${entityId}, using ${languageCode} label "${label.value}" instead!`);
                    labels.set(entityId, label.value);
                    break;
                }
            }
        }
    }
    shexElement.innerHTML = shexHTML
        .replace(
            entityIdsRegexp,
            function(match) {
                if (labels.has(match)) {
                    return `<abbr title="${labels.get(match).replace(/"/g, '&quot;')}">${match}</abbr>`;
                } else {
                    console.warn(`No label for ${match}!`);
                }
            }
        ).replace(
            /([^]?)(<span class="nn">wd<\/span><span class="p">:<\/span>|wd:)(<span class="nt">)?<abbr title="([^"]*)">(Q[1-9][0-9]*)<\/abbr>(<\/span>)?/g,
            function(_, lineend, wd, ntOpen = '', title, qid, ntClose = '') {
                let href, attr = '';
                if (lineend === '' || lineend === '\n') {
                    href = `http://www.wikidata.org/entity/${qid}`;
                    attr = ` id="${qid}"`;
                } else {
                    href = `#${qid}`;
                }
                return `${lineend}<a${attr} href="${href}">${wd}${ntOpen}<abbr title="${title}">${qid}</abbr>${ntClose}</a>`;
            }
        );

    const abbrStyle = document.createElement('style');
    abbrStyle.innerHTML = `
@media (hover: none), (-moz-touch-enabled) {
  abbr[title]::after {
    content: " (" attr(title) ")";
  }
}
`;
    document.head.appendChild(abbrStyle);
}

async function annotateWikidataEntityIdsInShex_default() {
    const shexElement = document.getElementById('shex');
    if (shexElement) {
        await annotateWikidataEntityIdsInShex(shexElement, navigator.languages.concat('en'));
    }
}

if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', annotateWikidataEntityIdsInShex_default);
} else {
    annotateWikidataEntityIdsInShex_default();
}
