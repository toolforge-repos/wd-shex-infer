# -*- coding: utf-8 -*-

from os import chmod, listdir, mkdir, path
import stat


class RejectJob(Exception):
    def __init__(self, rejected_job):
        self.rejected_job = rejected_job

class RejectJobDueToBlocks(RejectJob):
    def __init__(self, rejected_job, blocks):
        super().__init__(rejected_job)
        self.blocks = blocks

class RejectJobDueToPendingJobs(RejectJob):
    def __init__(self, rejected_job, pending_jobs):
        super().__init__(rejected_job)
        self.pending_jobs = pending_jobs

class JobManager:
    def __init__(self, job_store, job_runner, blocks_directory=None):
        self.job_store = job_store
        self.job_runner = job_runner
        self.blocks_directory = blocks_directory

        if blocks_directory is not None and not path.exists(blocks_directory):
            mkdir(blocks_directory, mode=0o1777) # this mode is subject to umask, so we follow up with chmod
            chmod(blocks_directory, stat.S_ISVTX | stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
            with open(path.join(blocks_directory, 'README'), 'w') as file:
                file.write("""
The existence of any file in this directory other than this README
will block the creation of any new jobs
in the Wikidata Shape Expressions Inference tool.
The contents of any such files will be shown to the users,
so you can leave a short message explaining why the tool had to be blocked.

cat > %s/T123456 << 'EOF'
We are experiencing high NFS error rates at the moment, see T123456.
EOF

To lift the block, simply delete the file again.

unlink %s/T123456
""".lstrip() % (blocks_directory, blocks_directory))

    def run(self, job):
        blocks = self.get_blocks()
        if blocks:
            raise RejectJobDueToBlocks(job, blocks)
        pending_jobs = self.get_pending_jobs()
        if len(pending_jobs) > 1: # don’t allow more than two concurrent jobs, see T201625
            raise RejectJobDueToPendingJobs(job, pending_jobs)
        job = self.job_store.job_new(job)
        self.job_runner.run(job)
        return job

    def get_by_id(self, id):
        job = self.job_store.get_job(id)
        if job is None:
            return None
        job = self._refresh_job(job)
        return job

    def get_blocks(self):
        if self.blocks_directory is None:
            return []
        blocks = []
        for entry in listdir(self.blocks_directory):
            if entry == 'README':
                continue
            try:
                with open(path.join(self.blocks_directory, entry), 'r') as file:
                    block = file.read(4096)
            except OSError:
                pass
            else:
                if len(block) == 4096:
                    block = block[:-1] + '…'
                blocks.append(block)
        return blocks

    def get_pending_jobs(self):
        pending_jobs = []
        for job in self.job_store.get_pending_jobs():
            job = self._refresh_job(job)
            pending_jobs.append(job)
        return pending_jobs

    def get_finished_jobs(self):
        # no need to refresh these
        return self.job_store.get_finished_jobs()

    def _refresh_job(self, job):
        if job.ts_first_stopped is not None:
            return job
        output = self.job_runner.get_output(job)
        if output is None:
            job = self.job_store.job_running(job)
        else:
            job = self.job_store.job_stopped(job, *output)
            self.job_runner.clean(job)
        return job
